# Adopta un arbol

## ¿Que es?
**Adopta un árbol UFRO** es un CPS para el monitoreo de árboles que están ubicado en el campus Andres Bello de la UFRO, para la recolección de datos relevantes de cada árbol y del ecosistema de la Universidad (por ejemplo: Calidad del aire, estado del árbol, etc.), cabe señalar que estos datos serán open data por tanto estarán disponible tanto para la Universidad, organismo medioambientales, los estudiantes, funcionarios y al público general, también existirá una app móvil que mostrará el estado de cada árbol del campus y que fomente a la comunidad universitaria a que “adopte” un árbol para que lo cuide, riegue, etc.

## ¿Como lo hace?
El CPS está conformado por una red de sensores ubicados en cada árbol del campus, un servidor en el cloud que recolecte los datos obtenidos por los sensores y una app móvil que muestre indicadores medioambientales tales como calidad del aire, la salud de cada árbol del campus, entre otros. Cada árbol tendrá incorporado un módulo ó caja y una red de sensores ubicados en distintas partes del árbol, estos sensores pueden ser un sensor de humedad, sensor de emisiones de carbono, sensor de savia, entre otros. El módulo tendrá un sistema de batería con recarga de energía solar y  un gasto de energía eficiente para mantener su funcionamiento 24/7, o la mayor parte del tiempo, también tendrá que resistir ambientes húmedos ya que estará expuesto al aire libre todo esto para mantener la capacidad de que los sensores se mantengan en funcionamiento para poder recolectar datos sobre los árboles y su estado actual.

**Dominios:** Cities, University, Environmental monitoring, Sustainability
