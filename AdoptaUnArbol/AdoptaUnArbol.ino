//#include <ArduinoMqttClient.h>
#include <PubSubClient.h>

#if defined(ARDUINO_SAMD_MKRWIFI1010) || defined(ARDUINO_SAMD_NANO_33_IOT) || defined(ARDUINO_AVR_UNO_WIFI_REV2)
#include <WiFiNINA.h>
#elif defined(ARDUINO_SAMD_MKR1000)
#include <WiFi101.h>
#elif defined(ARDUINO_ESP8266_ESP12)
#include <ESP8266WiFi.h>
#endif
#include "arduino_secrets.h"
#include "mqtt_secrets.h"
#include <SimpleDHT.h>
char ssid[] = SECRET_SSID; // your network SSID (name)
char pass[] = SECRET_PASS;     // your network password (use for WPA, or use as key for WEP)

// To connect with SSL/TLS:
// 1) Change WiFiClient to WiFiSSLClient.
// 2) Change port value from 1883 to 8883.
// 3) Change broker value to a server with a known SSL/TLS root certificate
// flashed in the WiFi module.
WiFiClient wifiClient;

int pos = 0;
const char broker[] = "mqtt3.thingspeak.com";
int port = 1883;
const char topic[] = "channels/1789122/publish"; // Canal "Adopta un arbol"
const char topicClientId[] = SECRET_MQTT_CLIENT_ID;
const char topicUsername[] = SECRET_MQTT_USERNAME;
const char topicPassword[] = SECRET_MQTT_PASSWORD;

void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
}
PubSubClient mqttClient(broker, port, callback, wifiClient);

// for DHT11,
//      VCC: 5V or 3
//      GND: GND
//      DATA: 2
#define pinSensorHumedadSuelo A1
#define pinSensorLluvia A2
#define pinSensorLDR A3
int pinDHT11 = 2;

SimpleDHT11 dht11(pinDHT11);

void setup()
{
  Serial.begin(115200);
  // attempt to connect to Wifi network:
  Serial.print("Attempting to connect to WPA SSID: ");
  Serial.println(ssid);
  while (WiFi.begin(ssid, pass) != WL_CONNECTED)
  {
    // failed, retry
    Serial.print(".");
    delay(5000);
  }
  Serial.println("You're connected to the network");
  Serial.println();
  // You can provide a unique client ID, if not set the library uses Arduino-millis()
  // Each client must have a unique client ID
  // mqttClient.setId("clientId");
  // You can provide a username and password for authentication
  // mqttClient.setUsernamePassword("username", "password");
  Serial.print("Attempting to connect to the MQTT broker: ");
  Serial.println(broker);
  if (!mqttClient.connect(topicClientId, topicUsername,topicPassword))
  {
    Serial.print("MQTT connection failed! Error code = ");
    //Serial.println(mqttClient.connectError());
    while (1)
      ;
  }
  Serial.println("You're connected to the MQTT broker!");
  Serial.println();
  Serial.print("Subscribing to topic: ");
  Serial.println(topic);
  Serial.println();
}

void loop()
{

  // start working...
  Serial.println("=================================");
  Serial.println("Sample DHT11...");

  // read without samples.
  byte temperature = 0;
  byte humidity = 0;
  int humidityEarth = map(analogRead(pinSensorHumedadSuelo), 0, 1023, 100, 0);
  int rainValue = map(analogRead(pinSensorLluvia),0,1023,0,100);
  int luminosidad = map(analogRead(pinSensorLDR),0,1023,100,0);
  // int humidityEarth = analogRead(pinSensorHumedadSuelo);
  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess)
  {
    Serial.print("Read DHT11 failed, err=");
    Serial.print(SimpleDHTErrCode(err));
    Serial.print(",");
    Serial.println(SimpleDHTErrDuration(err));
    delay(1000);
    return;
  }

  Serial.print("Sensor de Temp y Humedad: ");
  Serial.print((int)temperature);
  Serial.print(" *C, ");
  Serial.print((int)humidity);
  Serial.println(" H");
  
  Serial.print(humidityEarth);
  Serial.println("% Humedad del suelo");
  Serial.print(rainValue);
  Serial.println("Sensor Lluvia");
  Serial.print(luminosidad);
  Serial.println("Sensor Luminosidad");
  // DHT11 sampling rate is 1HZ.

  //Enviando mediciones mediante MQTT a Thinkspeak
  String payload = "field1=";
  payload.concat(temperature);
  payload.concat("&field2=");
  payload.concat(humidity);
  payload.concat("&field3=");
  payload.concat(rainValue);
  payload.concat("&field4=");
  payload.concat(luminosidad);
  payload.concat("&field5=");
  payload.concat(humidityEarth);
  payload.concat("&status=MQTTPUBLISH");
//  payload += "&field2="+humidity;
//  payload += "&field3="+rainValue;
//  payload += "&field4="+luminosidad;
//  payload += "&field5="+humidityEarth;
//  payload += "&status=MQTTPUBLISH";
    int largoPayload = payload.length() + 1;
    char buff[largoPayload];
    payload.toCharArray(buff,largoPayload);
    if(mqttClient.publish(topic,buff)){
      Serial.print("Mensaje enviado:");
      Serial.println(payload);
    }else{
      Serial.println("Error: El mensaje no fue enviado");
      }


  //mqttClient.publish(payload);
  delay(1500);
}
